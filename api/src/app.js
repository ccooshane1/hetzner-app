const express = require("express");
const {port, host} = require("./configuration");
const {connectDB} = require("./utils/db.js");
const {User} = require("./models/user");

const app = express();

connectDB()
 .on('error', console.error.bind(console, 'connection error:'))
 .once("open", startServer);

function startServer() {
    app.listen(port, () => {
     console.log(`Server is running on http://${host}:${port}`)
    });
 }

app.get('/users', async (req, res) => {
     try{
     const user = new User ({userName: "Anastasia"});
     await user.save();
     const users = await User.find();
     res.json(users);
        } catch (e) {
            res.send(e.message);
    }
});

app.get("/test", (req, res) => { 
 res.send("Server is working correctly")
 })